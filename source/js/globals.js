$(function() {
  var $body = $('html, body');

  $('.menu-toggle').on('click', function() {
    $(this)
      .siblings('.menu-sub')
      .slideToggle();
  });
  $('.navbar-open').on('click', function() {
    $('.navbar-mobile, .navbar-backdrop').addClass('is-show');
    $body.addClass('overflow-hidden');
  });
  $('.navbar-close, .navbar-backdrop').on('click', function() {
    $('.navbar-mobile, .navbar-backdrop').removeClass('is-show');
    $body.removeClass('overflow-hidden');
  });
});
