$('.js-quantity-btn').on('click', function() {
  var plus = $(this).data('plus');
  var input = $(this)
    .closest('.js-quantity')
    .find('.js-quantity-value');
  var value = input.val();
  var newValue = parseInt(value) + plus;
  if (newValue > 0) {
    input.val(newValue);
    input.trigger('change');
  }
});

function numberInput(className) {
  var input = $(className);
  input.keydown(function(e) {
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      return;
    }
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  });
}

function floating() {
  $('.floating').each(function() {
    var width = $(this).width();
    var offsetLeft = $(this).offset().left;
    var offsetTop = $(this).offset().top;
    $(this).data('offsetLeft', offsetLeft);
    $(this).data('offsetTop', offsetTop);
    $(this).css({
      width: width
    });
  });

  if ($(window).width() < 992) {
    return;
  }

  $(window).on('scroll', function() {
    $('.floating').each(function() {
      var top = 70;
      var scrollTop = $(window).scrollTop();
      var offsetTop = $(this).data('offsetTop');
      var offsetLeft = $(this).data('offsetLeft');
      var height = $(this).outerHeight();
      var outerHeight = $(this).outerHeight(true);
      var container = $(this).closest('.floating-container');
      var containerHeight = $(container).outerHeight();
      var containerOffsetTop = $(container).offset().top;

      if (outerHeight + offsetTop == containerHeight + containerOffsetTop) {
        return;
      } else if (scrollTop + top <= offsetTop) {
        $(this).css({
          position: 'static'
        });
      } else if (
        scrollTop + height + top >
        containerHeight + containerOffsetTop
      ) {
        $(this).css({
          position: 'absolute',
          zIndex: 2,
          top: 'auto',
          bottom: 0,
          left: '15px'
        });
      } else {
        $(this).css({
          position: 'fixed',
          zIndex: 2,
          top: top,
          left: offsetLeft,
          bottom: 'auto'
        });
      }
    });
  });
}

// file input
document.addEventListener('DOMContentLoaded', function() {
  $('.chosen-select').chosen({
    width: '100%',
    allow_single_deselect: true
  });

  Array.from(document.querySelectorAll('.file-input__input'), function(input) {
    input.addEventListener('change', function() {
      const name = this.value.split(/\\|\//).pop();
      const truncated = name.length > 40 ? name.substr(name.length - 40) : name;
      this.nextSibling.innerHTML = truncated;
    });
  });
});

// count To
(function($) {
  $.fn.countTo = function(options) {
    // merge the default plugin settings with the custom options
    options = $.extend({}, $.fn.countTo.defaults, options || {});

    // how many times to update the value, and how much to increment the value on each update
    var loops = Math.ceil(options.speed / options.refreshInterval),
      increment = (options.to - options.from) / loops;

    return $(this).each(function() {
      var _this = this,
        loopCount = 0,
        value = options.from,
        interval = setInterval(updateTimer, options.refreshInterval);

      function updateTimer() {
        value += increment;
        loopCount++;
        $(_this).html(value.toFixed(options.decimals));

        if (typeof options.onUpdate == 'function') {
          options.onUpdate.call(_this, value);
        }

        if (loopCount >= loops) {
          clearInterval(interval);
          value = options.to;

          if (typeof options.onComplete == 'function') {
            options.onComplete.call(_this, value);
          }
        }
      }
    });
  };

  $.fn.countTo.defaults = {
    from: 0, // the number the element should start at
    to: 100, // the number the element should end at
    speed: 1000, // how long it should take to count between the target numbers
    refreshInterval: 100, // how often the element should be updated
    decimals: 0, // the number of decimal places to show
    onUpdate: null, // callback method for every time the element is updated,
    onComplete: null // callback method for when the element finishes updating
  };
})(jQuery);

jQuery(function($) {
  $('.js-count').each(function() {
    var count = parseInt($(this).data('count'));
    $(this).countTo({
      from: 0,
      to: count,
      speed: 3000,
      refreshInterval: 5
    });
  });
});

// swiper-carousel-using-template
function addSwiper(selector, options = {}) {
  return Array.from(document.querySelectorAll(selector), function(item) {
    var $sliderContainer = $(item),
      $sliderEl = $sliderContainer.find('.swiper-container');

    if (options.navigation) {
      options.navigation = {
        prevEl: $sliderContainer.find(selector + '__prev'),
        nextEl: $sliderContainer.find(selector + '__next')
      };
    }

    if (options.pagination) {
      options.pagination = {
        el: $sliderContainer.find(selector + '__pagination'),
        clickable: true
      };
    }

    return new Swiper($sliderEl, options);
  });
}

/*!
 * jQuery plugin
 * What does it do
 */
// (function($) {
//   $.fn.floating = function(opts) {
//     // default configuration
//     var config = $.extend(
//       {},
//       {
//         item: '.js-floating-item',
//         marginTop: 20,
//         containerPadding: 15
//       },
//       opts
//     );

//     // main function
//     function floating(e) {
//       var $window = $(window);

//       if ($window.width() < 992) {
//         return;
//       }

//       $window.on('scroll', function() {
//         e.each(function() {
//           var $container = $(this),
//             containerHeight = $container.outerHeight(),
//             containerOffsetTop = $container.offset().top,
//             $item = $container.find(config.item),
//             itemHeight = $item.outerHeight(),
//             itemOuterHeight = $item.outerHeight(true),
//             itemOffsetTop = $item.offset().top,
//             scrollTop = $window.scrollTop(),
//             margin = config.marginTop,
//             padding = config.containerPadding;

//           if (
//             itemOuterHeight + itemOffsetTop ==
//             containerHeight + containerOffsetTop
//           ) {
//             return;
//           } else if (scrollTop + margin <= itemOffsetTop) {
//             $item.css('position', 'static');
//           } else if (
//             scrollTop + itemHeight + margin >
//             containerHeight + containerOffsetTop
//           ) {
//             $item.css({
//               width: $container.width(),
//               position: 'absolute',
//               zIndex: 2,
//               top: 'auto',
//               bottom: 0,
//               left: padding
//             });
//           } else {
//             $item.css({
//               width: $container.width(),
//               position: 'fixed',
//               zIndex: 2,
//               top: margin,
//               left: $container.offset().left,
//               bottom: 'auto'
//             });
//           }
//         });
//       });
//     }

//     // initialize every element
//     this.each(function() {
//       floating($(this));
//     });

//     return this;
//   };

//   // start
//   $(function() {
//     $('.js-floating').floating();
//   });
// })(jQuery);
